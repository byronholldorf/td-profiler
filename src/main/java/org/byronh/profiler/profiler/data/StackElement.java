package org.byronh.profiler.profiler.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayDeque;
import java.util.Deque;

public class StackElement {
	private String classname;

	private String method;

	private long usStart;

	private long usEnd;

	private Deque<StackElement> children = new ArrayDeque<>();

	StackElement() {

	}

	public StackElement(String classname, String method, long usStart, long time) {
		this.classname = classname;
		this.method = method;
		this.usStart = usStart;
		usEnd = usStart + time;
	}

	public String getClassname() {
		return classname;
	}

	public String getMethod() {
		return method;
	}

	public long getUsStart() {
		return usStart;
	}

	public long getUsEnd() {
		return usEnd;
	}

	public void setUsEnd(long UsEnd) {
		this.usEnd = UsEnd;
	}

	public StackElement updateOrInsertElement(String classname, String method, long usStart, long time) {
		if (!children.isEmpty()) {
			StackElement element = children.getLast();
			if (element.matches(classname, method)) {
				element.setUsEnd(usStart + time);
				return element;
			}
		}
		StackElement element = new StackElement(classname, method, usStart, time);
		children.addLast(element);
		return element;

	}

	private boolean matches(String classname, String method) {
    	return this.classname.equals(classname) && this.method.equals(method);
    }

	public Deque<StackElement> getChildren() {
		return children;
	}

	public void export(OutputStream os) throws IOException {
			String value = "{\"c\":\""+classname + "\",\"m\":\"" + method + "\",\"s\":" + usStart + ",\"l\":" + (usEnd - usStart);
			os.write(value.getBytes("ASCII"));
			
			if(!children.isEmpty()) {
				os.write(",\"e\":[".getBytes("ASCII"));
				boolean first = true;
				for(StackElement e : children) {
					if("".equals(e.classname)) {
						continue; // skip. This is a marker we don't need.
					}
					
					if(!first) {
						os.write(',');
					}
					first = false;
					e.export(os);
				}
				os.write("]".getBytes("ASCII"));
			}
			
			os.write('}');
	}

	public static StackElement importElement(InputStream in) throws IOException {
		
		StackElement se = new StackElement();

		se.classname = ParserUtil.read(in, '|');
		se.method = ParserUtil.read(in, '|');
		se.usStart = Long.parseLong(ParserUtil.read(in, '|'));
		se.usEnd = se.usStart + Long.parseLong(ParserUtil.read(in, '|'));
		
		while(true) {
			char next = (char)in.read();
			if(next == '@') {
				break;
			}
			se.children.addLast(importElement(in));
		}
		return se;
	}
	
}
