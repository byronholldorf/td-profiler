package org.byronh.profiler.profiler.data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class ThreadElementParseTest {

	@Test
	public void testExport() throws IOException {
		Map<Long, ThreadElement> threads = new HashMap<>();
		
		ThreadElement te = new ThreadElement("name", 1234L);
		StackElement se1 = new StackElement("class", "go", 1, 1);
		se1.setUsEnd(4);
		se1.getChildren().addLast(new StackElement("class", "go2", 2, 1));
		se1.getChildren().addLast(new StackElement("class", "go3", 3, 1));
		te.setStackElement(se1);
		threads.put(1234L, te);
		
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		ThreadElement.exportThread(threads, baos);
		
		Assert.assertEquals("[{i:1234,n:'name',e:"
				+ "{c:'class',m:'go',s:1,l:3,e:["
				+ "{c:'class',m:'go2',s:2,l:1},{c:'class',m:'go3',s:3,l:1}]}]", new String(baos.toByteArray()));
	}
	
	//@Test
	public void testBoth() throws IOException {
		String result= "[{i:1234,n:'name',e:"
				+ "{c:'class',m:'go',s:1,l:3,e:["
				+ "{c:'class',m:'go2',s:2,l:1},{c:'class',m:'go3',s:3,l:1}]}]";
		ByteArrayInputStream bais = new ByteArrayInputStream(result.getBytes("ASCII"));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Map<Long, ThreadElement> threads = ThreadElement.importThreads(bais);
		ThreadElement.exportThread(threads, baos);
		Assert.assertEquals(result, new String(baos.toByteArray()));
	}
}
