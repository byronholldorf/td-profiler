package org.byronh.profiler.profiler.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

public class ThreadElement {
	private long id;
	private String name;
	private StackElement stackElement;

	public ThreadElement(String name, long id) {
		this.id = id;
		this.name = name;
	}

	public void setStackElement(StackElement se) {
		stackElement = se;
	}

	public StackElement getStackElement() {
		return stackElement;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static void exportThread(Map<Long, ThreadElement> threads, OutputStream os) throws IOException {

		os.write('[');
		boolean first = true;
		for (ThreadElement element : threads.values()) {
			if(!first) {
				os.write(",\n\n".getBytes("ASCII"));
			}
			first = false;
			element.export(os);
		}
		os.write(']');
		os.flush();
	}

	public void export(OutputStream os) throws IOException {
		String value = "{\"i\":" + id + ",\"n\":\"" + name.replace("'", "\"") + "\",\"e\":";
		os.write(value.getBytes("ASCII"));
		stackElement.export(os);
		os.write('}');
	}

	public static Map<Long, ThreadElement> importThreads(InputStream bais) throws IOException {
		throw new UnsupportedOperationException("not yet");
//		Map<Long, ThreadElement> result = new HashMap<>();
//		
//		if(!bais.markSupported()) {
//			bais = new BufferedInputStream(bais);
//		}
//		
//		
//		while(bais.available() > 0) {
//			
//			long id = Long.parseLong(ParserUtil.read(bais, '|'));
//			String name = ParserUtil.read(bais, '|');
//			ThreadElement el = new ThreadElement(name, id);
//			el.setStackElement(StackElement.importElement(bais));
//			
//			result.put(el.id, el);
//				
//		}
//		
//		return result;
	}
}
