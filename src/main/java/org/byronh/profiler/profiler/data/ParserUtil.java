package org.byronh.profiler.profiler.data;

import java.io.IOException;
import java.io.InputStream;

class ParserUtil {
	static String read(InputStream in, char stop) throws IOException {
		StringBuilder sb = new StringBuilder();
		char c;
		while((c=(char) in.read()) != stop) {
			sb.append(c);
		}
		return sb.toString();
	}
}
