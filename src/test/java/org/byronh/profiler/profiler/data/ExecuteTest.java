package org.byronh.profiler.profiler.data;

import java.io.IOException;
import java.util.Map;

import org.byronh.profiler.profiler.ThreadDumpProfiler;

public class ExecuteTest {

	public static void main(String[] args) throws InterruptedException, IOException {
		ThreadDumpProfiler profiler = new ThreadDumpProfiler();
		
		profiler.runCapture();
		
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					while(true) {
						test();
					}
				} catch (Exception e) {
					// Don't care
				}
			}
		}, "stuff");
		t.setDaemon(true);;
		t.start();
		
		Thread.sleep(1000);
		profiler.stopCapture();
		
		Map<Long, ThreadElement> results = profiler.getResults();
		ThreadElement.exportThread(results, System.out);
	}
	
	public static void test() throws InterruptedException {
		switch((int)(Math.random()*4)) {
		case 0:
			test1(); break;
		case 1:
			test2(); break;
		case 2:
			test3(); break;
		case 3:
			test4(); break;
		}
	}
	
	public static void test1() throws InterruptedException {
		Thread.sleep(1);
		test2();
	}
	
	public static void test2() throws InterruptedException  {
		Thread.sleep(2);
		test3();
	}
	
	public static void test3() throws InterruptedException  {
		Thread.sleep(3); 
	}
	public static void test4() throws InterruptedException  {
		Thread.sleep(4);
	}

}
