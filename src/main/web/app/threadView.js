(function(){

angular.module('ThreadView', [])
.directive('threadView', function() {
    return {
        restrict: 'E',
        controller: 'ThreadViewController',
        controllerAs: 'tvc',
        templateUrl: 'app/threadView.tpl.html',
        scope: {
            threadName: '@',
            thread: '=',
            totalLength: '='
        }
    };
}).controller('ThreadViewController', ['$scope', function($scope) {
    $scope.height = 1000;
}])


.directive("drawing", function(){
  return {
    restrict: "A",
    link: function(scope, element){
        var ctx = element[0].getContext('2d');
        var vScale = .05;
        var WIDTH = 10;

        scope.getElementDimensions = function () {
            return { 'h': element[0].clientHeight, 'w': element[0].clientWidth };
        };

        scope.$watch(scope.getElementDimensions, function (newValue, oldValue) {
            element[0].height = element[0].clientHeight;
            element[0].width = element[0].clientWidth;
            h = element[0].height;
            w = element[0].width;
            vscale = h / scope.totalLength;
            ctx.setTransform(1, 0, 0, 1, 0, 0);
            draw.bkgd(ctx, w, h);
            ctx.translate(0.5, 0.5);
            draw.scale = vscale;
            //ctx.scale(vscale, vscale);
            drawElement(scope.thread.e, 0);
        }, true);

        element.bind('resize', function () {
            scope.$apply();
        });

        function drawElement(e, x) {
            draw.box(ctx, x, e.s, WIDTH / vscale, e.l, draw.color(e.c, e.m));
            if(e.e) {
                e.e.forEach(function(el) {
                    drawElement(el, x + WIDTH / vscale);
                });
            }
        }

//"c":"java.lang.Thread","m":"sleep","s":0,"l":98306,e":[{

    }
  };
});

})();