var draw = {
    scale: 1,
    bkgd: function(ctx, w, h) {
        var grd = ctx.createLinearGradient(0, 0, w, 0);
        grd.addColorStop(0, "#AAF");
        grd.addColorStop(1, "#EEE");

        ctx.fillStyle = grd;
        ctx.fillRect(0, 0, w, h);

    },

    box: function(ctx, x, y, w, h, col) {
        console.log("box("+x+","+y+","+w+","+h+")");
        ctx.strokeStyle = 'black';
        ctx.fillStyle=col;
        ctx.beginPath();
        ctx.rect(x * draw.scale,y * draw.scale,w * draw.scale,h * draw.scale);
        ctx.fill();
        ctx.stroke();
        
    },

    color: function(cls, method) {

        return 'hsl(' + (cls.hashCode()%360) + ','+((method.hashCode() %25) + 50)+'%,' + ((method.hashCode() %25) + 75) + '%)';
    }
}