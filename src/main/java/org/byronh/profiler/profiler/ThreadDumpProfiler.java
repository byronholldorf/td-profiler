package org.byronh.profiler.profiler;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import org.byronh.profiler.profiler.data.StackElement;
import org.byronh.profiler.profiler.data.ThreadElement;

public class ThreadDumpProfiler {
	private Map<Long, ThreadElement> threads = new HashMap<>();
	private long startTimeUs = 0;
	
	private double INTERVAL_OUT_RATIO = .95;

	private long usInterval = 10000;
	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1, new ThreadFactory() {
		@Override
		public Thread newThread(Runnable r) {
			Thread thread = new Thread(r, "profiler");
			thread.setDaemon(true);
			return thread;
		}
	});

	public ThreadDumpProfiler() {
	}
	
	private ScheduledFuture<?> scheduledCapture = null;
	
	public synchronized void runCapture() {
		if(scheduledCapture != null) {
			scheduledCapture.cancel(true);
		}
		threads.clear();

		scheduledCapture = scheduler.scheduleAtFixedRate(new Runnable() {
			@Override
			public void run() {
				try {
					capture();
				} catch (Exception e){
					e.printStackTrace();
				}
			}
		}, 0, usInterval, TimeUnit.MICROSECONDS);

	}
	
	public synchronized void stopCapture(){
		scheduledCapture.cancel(false);
		scheduledCapture = null;
		startTimeUs = 0;
	}
	
	public Map<Long, ThreadElement> getResults() {
		return threads;
	}
	
	public long getUsInterval() {
		return usInterval;
	}
	
	
	public void setUsInterval(long usInterval) {
		this.usInterval = usInterval;
	}
	
	private synchronized void capture() {
		Map<Thread, StackTraceElement[]> threadMap = Thread.getAllStackTraces();
		long usTime = System.nanoTime() / 1000;
		if(startTimeUs == 0) {
			startTimeUs = usTime;
		}
		usTime = usTime - startTimeUs;

		for (Entry<Thread, StackTraceElement[]> entry : threadMap.entrySet()) {
			Thread thread = entry.getKey();
			StackTraceElement[] elements = entry.getValue();

			StackElement se;
			if(elements.length > 0) {
				if (threads.containsKey(thread.getId())) {
					se = threads.get(thread.getId()).getStackElement();
					se.setUsEnd(usTime + (long)(usInterval * INTERVAL_OUT_RATIO));
				} else {
					ThreadElement t = new ThreadElement(thread.getName(), thread.getId());
					threads.put(thread.getId(), t);
					se = new StackElement(elements[0].getClassName(), elements[0].getMethodName(), usTime, (long)(usInterval * INTERVAL_OUT_RATIO));
					t.setStackElement(se);
				}
	
				for (int i = 1; i < elements.length; i++) {
					se = se.updateOrInsertElement(elements[i].getClassName(), elements[i].getMethodName(), usTime, (long)(usInterval * INTERVAL_OUT_RATIO));
				}
				se.updateOrInsertElement("", "", usTime, (long)(usInterval * INTERVAL_OUT_RATIO));
			}
		}
	}
}
