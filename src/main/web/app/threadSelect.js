(function(){

angular.module('ThreadSelect', [])
.directive('threadSelect', function() {
    return {
        restrict: 'AE',
        controller: 'ThreadSelectController',
        controllerAs: 'tsc',
        templateUrl: 'app/threadSelect.tpl.html'
    };
}).controller('ThreadSelectController', ['$scope', function($scope) {
    var vm = this;
    vm.data = [];
    vm.output = [];

    vm.load = function() {
        chrome.fileSystem.chooseEntry(function(file) {
            console.log(file);
            if(file) {
                file.file(function(file) {
                    var reader = new FileReader();

                    reader.onerror = function() {};
                    reader.onloadend = function(e) {
                        vm.data.length = 0;
                        vm.data = JSON.parse(e.target.result);
                        vm.output = [];
                    };

                    reader.readAsText(file);
                });
            }
        });
    };

    vm.totalLength = function() {
        var highest = 1000;
        vm.data.forEach(function(e) {
            if((e.e.s + e.e.l) > highest) {
                highest = (e.e.s + e.e.l);
            }
        });
        return highest + 1000;
    };
}]);

})();